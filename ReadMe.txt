=======================================
Projet Wedoogift - Nathan Lamirault
=======================================

Pour cet exercice, j'ai commencé par lire l'intégralité des consignes afin de comprendre globalement son intérêt.

Je me suis donc rendu compte que j'avais déjà réalisé ce type d'exercice (api rest en java), au cours d'un projet 
personnel pour la gestion de recettes et d'ingrédients.
Ainsi, j'ai donc repris la structure de mon ancien projet pour l'adapter aux consignes de l'exercice. 

Note : Il est possible que vous trouviez des traces du nom de l'ancien projet, à savoir "re7" pour recettes, 
mais j'ai fait en sorte que tout cela disparaisse au mieux sur la version finale du projet.

Temps de réalisation :  environ 7 heures. 
IDE utilisé : Eclipse pour le niveau 1, IntelliJ pour la fin. 
Logiciel pour tester l'API : Postman.


=======================================
Contenu de l'archive
=======================================

wedoogif-lvl1
--------------------

 Comme demandé dans la consigne du premier exercice, vous trouverez donc dans ce projet, l'implémentation des
 premières fonctions relatives au calcul de la balance et de l'attribution des cartes cadeau. 
 L'api est donc testable avec Postman avec les Url contenues dans les classes "Controller". 
 - exemple1 : localhost:8080/users --> avec la méthode GET, retournera la liste de tous les utilisateurs avec toutes leurs propriétés. 
 - exemple2 : localhost:8080/distributions --> avec la méthode POST, essayer d'ajouter une distribution. 
              Le corps de la requête devra être composé comme ceci : 
			 {
			      "amount": 100,
			      "start_date": "2021-09-16",
			      "end_date": "2022-09-15",
			      "company_id": 0,
			      "user_id": 2
		         }
              Il suffira ensuite d'afficher les users ou les distributions grâce à la méthode GET pour vérifier que l'appel a bien fonctionné.

 On notera ici qu'il y a un problème sur le calcul de la date dans la méthode, qui a été corrigée dans le niveau suivant. 
 La fonction permettant de calculer la balance de l'utilisateur est appelée à chaque fois qu'une distribution est effectuée. 


wedoogif-lvl2&3
--------------------

 Afin de gagner du temps, j'ai décidé ici de me concentrer uniquement sur l'API et de passer la génération du JSON. 
 Ici, il fallait dans un premier temps permettre de distribuer un nouveau type de bon, et d'en faire un nouveau portefeuille pour l'utilisateur. 
 
 J'ai donc adapté la fonction de calcul pour lui permettre de calculer uniquement le montant du portefeuille affecté par la distribution. 
 
 J'ai ajouté deux nouvelles classes "wallet" et "Balance", et adapté la méthode de distribution afin qu'elle agisse en conséquence des modifications. 
 (le corprs de la requête prendra maintenant un paramètre wallet_id)

 On peut ici, et de la même manière que dans les travaux précédents, tester l'API grâce à Postman et les urls contenues dans les classes Controller.

 Pour la dernière étape, j'ai essayé de réaliser des tests basiques sur le package User. 


=======================================
Des questions ? 
=======================================

Vous pourrez me contacter sur l'adresse mail avec laquelle j'ai envoyé ce mail si vous rencontrez des problèmes avec le projet. 
Je vous remercie pour l'attention que vous portez à ma candidature et espère pouvoir vous rencontrer bientôt.